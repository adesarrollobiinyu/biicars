﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tank : MonoBehaviour
{
    public string carName;
    public int health;
    public int attack;
    public int defense;
    public int transmision;

    public Tank(string carName,int health,int attack, int defense, int transmision)
    {
        this.carName = carName;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.transmision = transmision;
    }
}
