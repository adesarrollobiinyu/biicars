﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmController : MonoBehaviour
{
    public Rigidbody rbArm;
    Vector3 lookRot;

    // Start is called before the first frame update
    void Start()
    {
        rbArm.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        RayCastHit();
    }

    public void RayCastHit()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            lookRot = hit.point;
        }

        Vector3 lookDir = lookRot - transform.position;

        transform.LookAt(transform.position + lookDir, Vector3.up);
    }

}