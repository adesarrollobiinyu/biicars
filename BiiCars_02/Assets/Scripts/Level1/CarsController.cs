﻿#pragma warning disable 0649

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarsController : MonoBehaviour
{
    public Tank tank1;
    public Rigidbody rbCars;
    public float speed = 25f;

    [SerializeField]
    private bool active = false;

    // Start is called before the first frame update
    void Start()
    {
        rbCars.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (active == false )
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

            rbCars.AddForce(movement * speed);
        }
    }
}
