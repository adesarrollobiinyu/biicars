﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float m_dampTime = 0.27f;
    public float m_screenEdgeBuffer = 4f;
    public float m_minSize = 6.5f;

    public Transform[] m_Targets;

    public Camera m_Camera;
    public float m_MoveSpeed;
    public Vector3 m_moveVelocity;
    public Vector3 m_desiredPosition;

    //Awake is called inmediatally play de Editor
    public void Awake()
    {
        m_Camera = GetComponentInChildren<Camera>();
    }

    // Start is called before the first frame update
    public void Start()
    {
        
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
        Move();
        Zoom();
    }

    public void Move()
    {
        FindAveragePosition();
        transform.position = Vector3.SmoothDamp(transform.position, m_desiredPosition, ref m_moveVelocity, m_dampTime);
    }

    private void Zoom()
    {
        float requiredSize = FindRequiredSize();
    }

    private float FindRequiredSize()
    {
        Vector3 desiredLocalPosition = transform.InverseTransformPoint(m_desiredPosition);
        float size = 0f;

        for (int i = 0; i < m_Targets.Length; i++)
        {
            if (!m_Targets[i].gameObject.activeSelf)
            {
                continue;
            }

            Vector3 targetLocalPosition = transform.InverseTransformPoint(m_Targets[i].position);
            Vector3 desiredPositionToTarget = targetLocalPosition - desiredLocalPosition;

            size = Mathf.Max(size, Mathf.Abs(desiredPositionToTarget.y));
            size = Mathf.Max(size, Mathf.Abs(desiredPositionToTarget.x) / m_Camera.aspect);
        }

        size += m_screenEdgeBuffer;
        size = Mathf.Max(size, m_minSize);

        return size;
    }

    private void FindAveragePosition()
    {
        Vector3 averagePosition = new Vector3();
        int numTargets = 0;

        for (int i = 0; i < m_Targets.Length; i++) 
        {
            if (!m_Targets[i].gameObject.activeSelf)
            {
                continue;
            }

            averagePosition += m_Targets[i].position;
            numTargets++;
        }

        if (numTargets > 0)
        {
            averagePosition /= numTargets;
        }

        averagePosition.y = transform.position.y;
        m_desiredPosition = averagePosition;
    }

    public void SetStartPositionAndSize()
    {
        FindAveragePosition();
        transform.position = m_desiredPosition;
        m_Camera.orthographicSize = FindRequiredSize();
    }
}
