﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CarsSelector : MonoBehaviour
{
    public float speed;
    public GameObject[] carsList;
    private int index;

    // Start is called before the first frame update
    void Start()
    {
        index = PlayerPrefs.GetInt("CharacterSelected");

        carsList = new GameObject[transform.childCount];

        //Llenamos el array con nuestros modelos
        for (int i = 0; i < carsList.Length; i++)
        {
            carsList[i] = transform.GetChild(i).gameObject;
        }

        //Desactivamos de la Jerarquía
        foreach (GameObject go in carsList)
        {
            go.SetActive(false);
        }

        //Activamos desde el primer ítem de la lista (o primer carro seleccionado)
        if (carsList[index])
        {
            carsList[index].SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        RotationController();
    }

    public void ToggleLeft()
    {
        //Activar el modelo actual
        carsList[index].SetActive(false);
        index--;

        if (index < 0)
        {
            index = carsList.Length - 1;
        }

        //Activar el nuevo modelo
        carsList[index].SetActive(true);
    }

    public void ToggleRight()
    {
        //Activar el modelo actual
        carsList[index].SetActive(false);
        index++;

        if (index == carsList.Length)
        {
            index = 0;
        }

        //Activar el nuevo modelo
        carsList[index].SetActive(true);
    }

    public void RotationController()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("CarsSelector"))
        {
            if (Input.GetMouseButton(0))
            {
                transform.Rotate(new Vector3(0f, Input.GetAxis("Mouse X"), 0f));
            }
            else
            {
                transform.Rotate(new Vector3(0f, speed, 0f));
            }
        }

        //else
        //{
        //    transform.Rotate(new Vector3(0f, speed, 0f));
        //}
    }

    public void ConfirmButton()
    {
        PlayerPrefs.SetInt("CharacterSelected", index);
        SceneManager.LoadScene("Level1");
    }
}
